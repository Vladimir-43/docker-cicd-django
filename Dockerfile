FROM python:3-alpine
WORKDIR /app
RUN mkdir db
COPY ./requirements.txt .
RUN pip install -r requirements.txt 
COPY . .
EXPOSE 8000
CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
